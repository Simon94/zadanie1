#pragma once
#include <string>
#include <iostream>
using namespace std;
class atrakcja
{
private:
	int cena;
	string nazwa;
	string opis;
public:
	void wyswietl();

	atrakcja(int ,string,string);
	~atrakcja();
};
class kolejka :public atrakcja
{
private:
	string godz_odjazdu;
	string godz_przyjazdu;
public:
	void wyswietl_kolejka();
	void inicjuj(int ,string,string ,string ,string );
	kolejka();
	~kolejka();

private:

};
class zamek :public atrakcja
{
private:
	int czas_zwiedzania;
public:
	void wyswietl_zamek();
	void inicjuj(int , string , string , int);
	zamek();
	~zamek();

private:

};
class film :public atrakcja
{
private:
	int czas_trwania;
	string tytul;
public:
	void wyswietl_film();
	void inicjuj(int, string, string, int, string);
	film();
	~film();

private:

};



